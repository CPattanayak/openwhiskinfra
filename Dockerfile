FROM python:3.6.6 as build_env
RUN pip3 install pyeda




FROM openwhisk/python3action

# lapack-dev is available in community repo.
RUN echo "http://dl-4.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories

# add package build dependencies
# RUN apk add --no-cache g++ linux-headers libc-dev musl-dev

COPY --from=build_env /usr/local/lib/python3.6/site-packages/pyeda /usr/local/lib/python3.6/site-packages/pyeda 

# add python packages
RUN pip3 install pymongo
RUN pip3 install neo4j-driver


